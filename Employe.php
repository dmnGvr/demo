<?php 
/** Classe Employe en PHP */

class Employe{
    /** Identification unique d'un employe */ 
    private $id; 
    /** Nom de l'employe */ 
    private $nom;
    
    public function __construct($ident, $nom){  // constructeur
        $this->id=$ident;
        $this->nom=$nom;
    }

    public function setId($ident){      // setter
        $this->id=$ident;
    }
    public function setNom($n){         // setter
        $this->nom=$n;
    }

    public function getId(){            // getter
         return $this->id;
    }
    public function getNom(){           // getter
        return $this->nom;
    }
}